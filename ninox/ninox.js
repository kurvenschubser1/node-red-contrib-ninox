/**
 * Copyright 2019 Malte Engelhardt.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

module.exports = function (RED) {
  'use strict';
  var fs = require("fs");
  var path = require("path");
  var request = require("request");
  var nxlib = require("./lib");
  
  function NinoxConfigNode(n) {
    RED.nodes.createNode(this, n);
    let node = this;
    node.options = {
      "host": n.host,
      "port": n.port,
      "apiversion": n.apiversion,
      "apikey": n.apikey,
      "publicprivate": n.publicprivate, // true == public, false == private
      "team": n.team,
      "teamselect": n.teamselect,
      "teamname": n.teamname
    };
  }

  RED.nodes.registerType('ninox-config', NinoxConfigNode);

  function NinoxActionNode(n) {
    RED.nodes.createNode(this, n);
    let node = this;
    node.nxcfg = n.config;
    node.cfg = RED.nodes.getNode(node.nxcfg);


    let responseHandlerForMsg = function(msg) {
      return function(err, resp, body) {
        console.log("resp:", err, resp.statusMessage);
        if (resp.statusCode !== 200 && resp.statusCode !== 204) {
          node.status({
            text: "Server error: " + resp.statusMessage + (err ? " " + JSON.stringify(err) : "")
          });
          node.error("Server error: ", resp.statusMessage + (err ? " " + JSON.stringify(err) : ""));
        }
        else {
          console.log("responsehandler", resp.headers);
          if (resp.statusCode === 204) {
            body = { "success": true };
          }
          else if (resp.headers["content-type"].match("application/json")) {
            if (typeof body === "string") {
              try {
                body = JSON.parse(body);
              }
              catch(e) {
                console.log("error parsing json:", err, body);
              }
            }
            else {
              // body probably is an Object object already
            }
          }
          msg.payload = body;
          node.send(msg);
        }
      };
    };

    node.on('input', function (msg) {
      if (node.cfg) {
        node.method = n.method;
        node.database = n.database;
        node.table = n.table;
        node.map = n.map;
        node.types = n.types;
        
        let url = nxlib.getUrl(node.cfg.options) + `/${node.database}/tables/${node.table}/`;
        let headers = nxlib.getHeaders(node.cfg.options);
        let responseHandler = responseHandlerForMsg(msg);

        if (node.method === "all") {
          url += "records";

          let page = 0;
          let perpage = 250;
          let combinedreturn = msg.combinedreturn || false;
          let maxpages = -1;
          if (typeof msg.page !== "undefined") {
            let parsedInt = parseInt(msg.page);
            if (!isNaN(parsedInt)) {
              page = parsedInt;
              maxpages = 1;
            }
          }
          if (msg.perpage) {
            let parsedInt = parseInt(msg.perpage);
            if (!isNaN(parsedInt)) {
              perpage = parsedInt;
            }
          }
          if (msg.maxpages) {
            let parsedInt = parseInt(msg.maxpages);
            if (!isNaN(parsedInt)) {
              maxpages = parsedInt;
            }
          }
          let res = new nxlib.NxAPIResult("get", url, headers, "", page, perpage, maxpages);
          (async function() {
            let rv = [];
            try {
              for await (const rec of res) {
                if (!combinedreturn) {
                  node.send({ payload: rec, complete: false });
                }
                else {
                  rv.push(rec);
                }
              }
            }
            catch(err) {
              console.log(err.message);
              node.status({ text: err.message });
            }
            finally {
              if (!combinedreturn) {
                node.send({ complete: true });
              }
              else {
                node.send( { payload: rv, complete: true });
              }
            }
          })();
        }
        else if (node.method === "one") {
          let nid = msg.payload;
          url += `records/${nid}`;
          console.log("url", url, "headers", headers);
          request({ url: url, headers: headers }, responseHandler);
        }
        else if (node.method === "find") {
          url += "record";
          const combinedmapdef = [{ "fields": {} }];
          for (let m of node.map) {
            combinedmapdef[0].fields[m.key] = m.value;
          }
          const ctx = { msg };
          const resolvemap = nxlib.resolveValues(combinedmapdef, ctx);
          const typemap = node.types.map((v,i,a) => ({ "key": v.id, "name": v.name, "value": null }));
          const mapped = nxlib.applyMap([{ "fields": msg.payload }], typemap);
          const body = Object.assign({}, resolvemap[0].fields, mapped[0].fields);
          const opts = { url: url, headers: headers, json: true, body: body };
          console.log("find:", opts, mapped);
          request.post(opts, responseHandler);
        }
        else if (node.method === "save") {
          url += "records";
          const combinedmapdef = [{ "fields": {} }];
          for (let m of node.map) {
            combinedmapdef[0].fields[m.key] = m.value;
          }
          const ctx = { msg };
          const resolvemap = nxlib.resolveValues(combinedmapdef, ctx);
          const rmfs = resolvemap[0].fields;
          const typemap = node.types.map((v,i,a) => ({ "key": v.id, "name": v.name, "value": null }));
          const mapped = nxlib.applyMap(msg.payload, typemap);
          for (let p of mapped) {
            for (let k of Object.keys(rmfs)) {
              if (typeof p.fields[k] === "undefined") {
                p.fields[k] = rmfs[k];
              }
            }
          }
          let opts = { url: url, headers: headers, json: true, body: mapped };
          request.post(opts, responseHandler);
        }
        else if (node.method === "delete") {
          let nid = msg.payload;
          url += `records/${nid}`;
          request.delete({ url: url, headers: headers }, responseHandler);
        }
        else if (node.method === "allfiles") {
          let nid = msg.payload;
          url += `records/${nid}/files`;
          request.get({ url, headers }, responseHandler);
        }
        else if (node.method === "onefile") {
          const nid = msg.nid;
          const rnd = Math.random().toString(36).substring(2, 15);
          const tmpdir = `tmpfiles/${rnd}`;
          const fname = `${tmpdir}/${msg.filename}`;
          url += `records/${nid}/files/${msg.filename}`;
          // TODO: use javascript instead of curl to download file
          // request.get({ url, headers }, responseHandlerFile);
          const exec = require('child_process').exec;
          const cmd = `mkdir -p ${tmpdir}; curl --header "Authorization: ${headers["Authorization"]}" ${url} -o ${fname};`;
          exec(cmd, (e, stdout, stderr) => {
            if (e instanceof Error) {
              throw e;
            }
            msg.pathname = fname;
            node.send(msg);
          });
        }
        else if (node.method === "uploadfile") {
          let nid = msg.nid;
          let fname = msg.filename;
          url += `records/${nid}/files`;
          headers["Content-Type"] = "multipart/form-data";
          let formData = {};
          // have payload be streamable
          if (msg.payload && typeof msg.payload.pipe === "function" && typeof msg.payload.on === "function") {
            formData[path.basename(fname)] = msg.payload;
          }
          else {
            formData[path.basename(fname)] = fs.createReadStream(fname);
          }
          request.post({ url, headers, formData }, responseHandler);
        }
        else if (node.method === "deletefile") {
          let nid = msg.nid;
          let fname = msg.filename;
          url += `records/${nid}/files/${fname}`;
          request.delete({ url, headers }, responseHandler);
        }
      } else {
        node.error('missing ninox configuration');
      }
    });
    node.status({
      text: "Ready"
    });
  }
  RED.nodes.registerType('ninox action', NinoxActionNode);
};
