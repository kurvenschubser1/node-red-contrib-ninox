/**
 * Copyright 2019 Malte Engelhardt.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

var requestpromisenative = require("request-promise-native");
const NX_NONE_TOKEN = "<^>[.~.]none[-|-]]<_>";
function lookupFieldID(map, fieldname) {
    for (let m of map) {
        if (m.name === fieldname) {
            return m.key;
        }
    }
    return NX_NONE_TOKEN;
}

module.exports = {
    NX_PUBLIC_CLOUD_API_HOSTNAME: "api.ninoxdb.de",
    NX_PUBLIC_CLOUD_API_PORT: "443",
    NxAPIResult: class {
        constructor(verb, url, headers, body = "", page = 0, perpage = 250, maxpages = -1) {
            this.verb = verb;
            this.url = url;
            this.headers = headers;
            this.body = body;
            this.page = page;
            this.perpage = perpage;
            this.maxpages = maxpages;
            this.currpage = page;
            this.currrec = -1;
            this.pagerecords = [];
            this.pagesseen = 0;
            this.allrecs = 0;
        }

        [Symbol.asyncIterator]() {
            return this;
        }

        loadpage(page, perpage) {
            let url = this.url + `?page=${page}&perPage=${perpage}`;
            const opts = {url, headers: this.headers};
            if (this.body !== "") {
                opts.body = this.body;
            }
            return requestpromisenative[this.verb](opts);
        }

        async next() {
            if (this.pagerecords.length < this.perpage && this.currrec >= this.pagerecords.length || 
                this.maxpages > -1 && this.pagesseen > this.maxpages ||
                this.maxpages === 0 ||
                (this.maxpages > -1 ? this.allrecs >= this.maxpages * this.perpage : false)) {
                return Promise.resolve({ done: true });
            }
            if (this.currrec === -1) {
                this.currrec = this.perpage;
            }
            if (this.currrec >= this.perpage) {
                let req = this.loadpage(this.currpage, this.perpage);
                let resp = await req;
                this.currpage++;
                this.pagesseen++;
                this.currrec = 0;
                this.pagerecords = resp && JSON.parse(resp) || [];
            }
            console.log("next", "currpage", this.currpage, "currrec", this.currrec);
            if (this.pagerecords.length === 0) {
                return Promise.resolve({ done: true });
            }
            this.allrecs++;
            return Promise.resolve({ value: this.pagerecords[this.currrec++], done: false });
        }
    },
    getUrl: function(node, doGetTeams = false) {
        let port = (node.port && node.port !== "443" ? ":" + node.port : "");
        let bits = [
            "https://" + node.host + port
        ];
        let publicprivate = node.publicprivate;
        if (publicprivate) {  // public cloud host
            bits.push(node.apiversion);
            bits.push("teams");
            if (!doGetTeams) {  // we want anything after "teams"
              bits.push(node.team);
              bits.push("databases");
            }
        }
        else {  // private cloud or on-premise host, http://host:port/teamid/api/v1/databases
            if (doGetTeams) {
              throw new Error("ProgrammingError: Private cloud does not allow to query teams.");
            }
            bits.push(node.team);
            bits.push("api");
            bits.push(node.apiversion);
            bits.push("databases");
        }
        if (bits.indexOf("none") > -1) {
          bits.splice(bits.indexOf("none"));
        }
        return bits.filter(v => !!v).join("/");
    },
    getHeaders: function(node) {
      return {
          "Authorization": "Bearer " + node.apikey,
          "Content-Type": "application/json"
      };
    },
    applyMap: function(payload, map) {
        let mapped = [];
        for (let p of payload) {
            let cpy = Object.assign({}, p);
            cpy.fields = Object.assign({}, p.fields);
            for (let k of Object.keys(cpy.fields)) {
                let fid = lookupFieldID(map, k);
                if (fid !== NX_NONE_TOKEN) {
                    cpy.fields[fid] = cpy.fields[k];
                    delete cpy.fields[k];
                }
            }
            mapped.push(cpy);
        }
        return mapped;
    },
    resolveValues: function(payload, context) {
        let resolved = [];
        for (let p of payload) {
            let cpy = Object.assign({}, p);
            cpy.fields = Object.assign({}, p.fields);
            for (let k of Object.keys(cpy.fields)) {
                let v = cpy.fields[k];
                if (!isNaN(parseFloat(v))) {
                    // it's a number, no transform required
                }
                else if (v === "false") {
                    v = false;
                }
                else if (v === "true") {
                    v = true;
                }
                else if ((v.startsWith('"') && v.endsWith('"')) || (v.startsWith("'") && v.endsWith("'"))) {
                    v = v.substr(1, v.length - 2);
                }
                else {
                    let bits = cpy.fields[k].split(".");
                    let curr = context;
                    for (let bit of bits) {
                        curr = curr[bit];
                    }
                    v = curr;
                }
                cpy.fields[k] = v;
            }
            resolved.push(cpy);
        }
        return resolved;
    }
};
